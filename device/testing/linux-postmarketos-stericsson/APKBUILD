# Reference: <https://postmarketos.org/vendorkernel>
# Maintainer: Newbyte <newbyte@disroot.org>
# Kernel config based on: arch/arm/configs/u8500_defconfig

_flavor="postmarketos-stericsson"
_config="config-$_flavor.armv7"
pkgname=linux-$_flavor
pkgver=5.16.7
pkgrel=0
pkgdesc="Mainline kernel fork for ST-Ericsson NovaThor devices"
arch="armv7"
_carch="arm"
url="https://github.com/stericsson-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-anbox
	pmb:kconfigcheck-containers
	pmb:kconfigcheck-zram
	pmb:kconfigcheck-nftables
	"
makedepends="
	bison
	findutils
	gmp-dev
	flex
	mpc1-dev
	mpfr-dev
	openssl-dev
	perl
	postmarketos-installkernel
"
case $pkgver in
	*.*.*)	_kernver=${pkgver%.0};;
	*.*)	_kernver=$pkgver;;
esac
source="
	https://cdn.kernel.org/pub/linux/kernel/v${_kernver%%.*}.x/linux-$_kernver.tar.xz
	config-$_flavor.armv7
	emmc.patch
	fix-boot-regression-skomer.patch
"
builddir="$srcdir/linux-${_kernver//_/-}"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="
db21a01720921ebca5d462b828c661be3edcd0c6f62e20a187aff95b1f8579b119cc7d5443fac14f9372dba41764cf01b50a484eabfad33dcb6b06d2e1268a17  linux-5.16.7.tar.xz
ccd9280c1398425884998c0c9324284738a5b64f55c78a391aaa78c0e030fed11d6386ee44396cd893c006047ba86f4ce84dc7ef5faf98ecd52cd57b84ca6a07  config-postmarketos-stericsson.armv7
d1bd4c1f04e67ab33f03285f8baf1187d4dde29b9c49711c56f31d4708296172295ea1ad153504f2a4e944ad3d80ddc12a7218dc0d79161089ed527fc4404632  emmc.patch
a45735c71cedb53359f9b26a9d1cb6c15b850eb871c21ad5d5414cfe83a0186efaa86dcbff746a9327da1e8e4edba9d439454c7743684a6429167a93f83febf4  fix-boot-regression-skomer.patch
"
